### Quick start

# clone the repo
# --depth 1 removes all but one .git commit history
git clone --depth 1 https://bitbucket.org/steedlover/betent.git

# install the repo with npm and bower
* bower install
* npm install

# start the server
npm run start
