app.directive('circle', function($animate) {
	return {
		scope: {
			clickFunction: '&' // binding the callback function what trigers on circle clicking
			// other parameters get from observing of directive html attributes
		},
		template: '<div class="circle" ng-click="click()"></div>',
		link: function(scope, elem, attrs) {

			var circleEl = elem.children(':first');

			attrs.$observe('color', function(newVal) {
				circleEl.css('background-color', '#' + newVal);
			});

			attrs.$observe('coords', function(newVal) {
				var coordsArr = newVal.split(',');
				if (coordsArr.length >= 2) {
					// here array elements could be check for type of integer, but for this example it would be redundantly
					if (coordsArr[1] > 0) {
						$animate.addClass(circleEl, "right");
					} else {
						$animate.removeClass(circleEl, "right");
					}
					if (coordsArr[0] > 0) {
						$animate.addClass(circleEl, "bottom");
					} else {
						$animate.removeClass(circleEl, "bottom");
					}
					//scope.$apply();
				}
			});

			scope.click = function() {
				// In this case this check is useless but for custom directives in general it's useful to check the existence of functions before execution
				if (_.isFunction(scope.clickFunction)) {
					scope.clickFunction();
				}
			};

		}
	};
});
