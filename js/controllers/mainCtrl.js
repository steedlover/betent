/* globals _ */
// Here I say to JSHint/JSLint the _ variable has been defined already

app.controller('myCtrl',

	function($scope, $http) {

		var cPath = 'http://www.colr.org/json/color/random';

		// variable shows the ready of the data (getting the color from API)
		$scope.ready = false;
		// Coordinats of the ball by percentage for all positions within the container
		$scope.posArr = [
			'0,0',
			'0,100',
			'100,100',
			'100,0'
		];
		// number of element of the coords array by default
		$scope.curPos = 0;

		var getNewColor = function() {
			return $http.get(cPath);
		};

		var nextPos = function() {
			if ($scope.curPos < $scope.posArr.length - 1) {
				$scope.curPos++;
			} else {
				$scope.curPos = 0;
			}
		};

		var setNewColor = function(color) {
			$scope.colorObj = color;
		};

		$scope.circleClick = function() {
			getNewColor().then(function(res) {
				if (_.isObject(res) && _.isObject(res.data) && Array.isArray(res.data.colors) && res.data.colors.length > 0) {
					setNewColor(res.data.colors[0]);
				}
				nextPos();
			});
		};

		// Start controller with init of all variables
		getNewColor().then(function(res) {
			$scope.ready = true;

			// Complicated checks prevent of unexpected errors but looks not very gracefully
			if (_.isObject(res) && _.isObject(res.data) && Array.isArray(res.data.colors) && res.data.colors.length > 0) {
				setNewColor(res.data.colors[0]);
			}
		});

	}

);
